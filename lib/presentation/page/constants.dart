import 'package:flutter/material.dart';

const textStyle = TextTheme(
  headline1: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
  headline2: TextStyle(
      fontSize: 15, fontWeight: FontWeight.bold, fontFamily: 'Georgia'),
  headline3: TextStyle(
      fontSize: 12, fontFamily: 'Georgia', fontStyle: FontStyle.italic),
);
const appBarTheme = AppBarTheme();
const kPrimaryColor = Color(0xFF00BF6D);
const kSecondaryColor = Color(0xFFFE9901);
const kContentColorLightTheme = Color(0xFF1D1D35);
const kContentColorDarkTheme = Color(0xFFF5FCF9);
const kWarninngColor = Color(0xFFF3BB1C);
const kErrorColor = Color(0xFFF03738);

const kDefaultPadding = 20.0;
