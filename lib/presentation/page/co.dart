import 'package:easybuy/data/models/categoryModel.dart';
import 'package:easybuy/presentation/page/homeBottomNavBar.dart';
import 'package:easybuy/presentation/page/BottomParPages/main_page.dart';
import 'package:easybuy/presentation/page/homeBottomNavBar1.dart';
import 'package:easybuy/translations.dart';
import 'package:flutter/material.dart';
import 'package:scroll_snap_list/scroll_snap_list.dart';

class Home extends StatefulWidget {
  Home({Key key, this.title}) : super(key: key);

  final String title;
  static List<CatModel> getcats() {
    List<CatModel> list = [];
    list.add(CatModel(null, 'assets/icons/car.png', 'first'));
    list.add(CatModel(null, 'assets/icons/car.png', 'second'));
    list.add(CatModel(null, 'assets/icons/car.png', 'third'));
    list.add(CatModel(null, 'assets/icons/car.png', 'fourth'));
    list.add(CatModel(null, 'assets/icons/car.png', 'fifth'));
    list.add(CatModel(null, 'assets/icons/car.png', 'sixth'));
    list.add(CatModel(null, 'assets/icons/car.png', 'seventh'));
    return list;
  }

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<Home> {
  String valueChoose;

  List<String> listItem = ["test1", "test2"];

//
  List<int> data = [10, 9, 8, 7, 6, 5, 4, 3, 2, 1];

  Widget _buildItemList(BuildContext context, int index) {
    if (index == data.length)
      return Center(
        child: CircularProgressIndicator(),
      );
    return GestureDetector(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => HomeBottomNavBar1(Home.getcats(), '')));
      },
      child: Container(
        width: 150,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Center(
              child: Container(
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage('images/uaef.png'),
                        fit: BoxFit.cover)),
                width: 135,
                height: 135,
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    // FlutterStatusbarcolor.setStatusBarColor(Color(0xffff));
    return Scaffold(
      resizeToAvoidBottomInset: true,
      backgroundColor: Colors.white,
      body: Container(
        height: MediaQuery.of(context).size.height,
        child: new SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Container(
                padding: EdgeInsets.fromLTRB(10, 45, 0, 0),
                //height: 274.5,
                height: MediaQuery.of(context).size.height / 2.7,
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage('images/eee.png'), fit: BoxFit.fill)),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      Translations.of(context).text('slog'),
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.normal,
                          fontSize: 25),
                    ),
                    Text(
                      "",
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.normal,
                          fontSize: 30),
                    ),
                  ],
                ),
              ),
              //
              Padding(
                padding: EdgeInsets.only(top: 25),
              ),
              Container(
                width: 260,
                child: Container(
                  child: Container(
                    height: 52,
                    padding: EdgeInsets.symmetric(horizontal: 18),
                    decoration: BoxDecoration(
                      color: Color.fromRGBO(235, 235, 235, 100),
                      borderRadius: BorderRadius.circular(50),
                      border: Border.all(
                        color: Colors.black,
                        style: BorderStyle.solid,
                        width: 0,
                      ),
                    ),
                    child: DropdownButton(
                      hint: Text("Choose your Language"),
                      isExpanded: true,
                      style: TextStyle(
                          color: Color.fromRGBO(235, 235, 235, 100),
                          fontWeight: FontWeight.normal,
                          fontSize: 16),
                      value: valueChoose,
                      onChanged: (newValue) {
                        setState(() {
                          valueChoose = newValue;
                        });
                      },
                      items: listItem.map((valueItem) {
                        return DropdownMenuItem(
                          value: valueItem,
                          child: Text(
                            valueItem,
                            style: TextStyle(color: Colors.black),
                          ),
                        );
                      }).toList(),
                    ), //getDropdownButton(context, valueChoose, listItem),
                  ),
                ),
              ),
              //
              Padding(
                padding: EdgeInsets.only(top: 65),
              ),
              Container(
                height: 500, //double.infinity,
                alignment: Alignment.bottomCenter,
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                      colors: [
                        Colors.orange,
                        Colors.orange,
                        Colors.red,
                      ],
                      begin: Alignment.centerLeft,
                      end: Alignment.centerRight,
                    ),
                    borderRadius: const BorderRadius.only(
                        topLeft: Radius.circular(80),
                        topRight: Radius.circular(80)),
                    // border: Border.all(
                    //   color: Colors.black,
                    //   style: BorderStyle.solid,
                    //   width: 0,
                    // ),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.black.withOpacity(0.2),
                        spreadRadius: 8,
                        blurRadius: 10,
                        offset: Offset(0, 3),
                      )
                    ]),
                child: Column(
                  children: [
                    Padding(
                      padding: EdgeInsets.only(top: 20),
                    ),
                    new Text(
                      "Choose your Country",
                      style: TextStyle(
                        fontFamily: "Netflix",
                        fontWeight: FontWeight.w600,
                        fontSize: 22,
                        letterSpacing: 0.0,
                        color: Colors.white,
                      ),
                    ),
                    Expanded(
                      child: ScrollSnapList(
                        itemBuilder: _buildItemList,
                        itemSize: 150,
                        dynamicItemSize: true,
                        onReachEnd: () {},
                        itemCount: data.length,
                        onItemFocus: (int) {},
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget getDropdownButton(context, valueChoose, List<String> listItem) {
    return DropdownButton(
      hint: Text("Choose your Language"),
      isExpanded: true,
      style: TextStyle(
          color: Color.fromRGBO(235, 235, 235, 100),
          fontWeight: FontWeight.normal,
          fontSize: 16),
      value: valueChoose,
      onChanged: (newValue) {
        setState(() {
          valueChoose = newValue;
        });
      },
      items: listItem.map((valueItem) {
        return DropdownMenuItem(
          value: valueItem,
          child: Text(
            valueItem,
            style: TextStyle(color: Colors.black),
          ),
        );
      }).toList(),
    );
  }
}
