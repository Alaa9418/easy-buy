import 'dart:async';

import 'package:carousel_slider/carousel_slider.dart';
import 'package:easybuy/presentation/page/constants.dart';
import 'package:flutter/material.dart';
import 'package:easybuy/presentation/page/ProductDetailsPage.dart';

class FeaturesSlider extends StatelessWidget {
  int duration;

  FeaturesSlider(this.duration);
  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;
    return CarouselSlider(
      items: [
        GestureDetector(
          onTap: () {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => ProductDetailsPage()));
          },
          child: Container(
            alignment: Alignment.center,
            // height: height / 4,
            width: width / 2.7,
            // margin: EdgeInsets.only(right: 10),
            decoration: BoxDecoration(
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.5),
                  spreadRadius: 2,
                  blurRadius: 5,
                  offset: Offset(0, 3), // changes position of shadow
                ),
              ],
              border: Border.all(width: 1, color: Colors.grey),
              borderRadius: BorderRadius.circular(10.0),
            ),
            child: Column(
              children: [
                //image con
                getImageUI(height),
                // getDateUI(),
                getNameUI(),
                getDescriptionUI(),
                Container(
                  height: 1,
                  color: Colors.grey[400],
                  width: double.infinity,
                  margin: EdgeInsets.only(left: 25, right: 25),
                ),
                getPriceUI(5000),
              ],
            ),
          ),
        ),
      ],
      options: CarouselOptions(
        height: 200.0,
        // disableCenter: true,
        viewportFraction: 0.43,
        autoPlay: true,
        autoPlayInterval: const Duration(seconds: 3),
        autoPlayAnimationDuration: Duration(milliseconds: duration),
        autoPlayCurve: Curves.fastOutSlowIn,
      ),
    );
  }

  Widget getImageUI(height) {
    return Container(
      height: height / 7,
      width: double.infinity,
      decoration: BoxDecoration(
          color: Colors.red,
          image: DecorationImage(
            // image: AssetImage(
            //   'images/uaef.png',
            // ),
            image: NetworkImage(
                'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/2022-chevrolet-corvette-z06-1607016574.jpg?crop=0.737xw:0.738xh;0.181xw,0.218xh&resize=640:*'),
            fit: BoxFit.fill,
          ),
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(10), topRight: Radius.circular(10))),
    );
  }

  Widget getDateUI() {
    return Container(
      width: double.infinity,
      padding: EdgeInsets.only(left: 25, top: 5),
      child: Row(
        children: [
          Icon(
            Icons.calendar_today_outlined,
            size: 15, //make a theme for it
          ),
          Text(
            '15/4/2021',
            textAlign: TextAlign.left,
            textWidthBasis: TextWidthBasis.longestLine,
            style: textStyle.headline3,
          ),
        ],
      ),
    );
  }

  Widget getNameUI() {
    return Container(
      padding: EdgeInsets.only(left: 20, top: 5),
      width: double.infinity,
      child:
          Text('suzuki', style: textStyle.headline2, textAlign: TextAlign.left),
    );
  }

  Widget getDescriptionUI() {
    return Container(
      width: double.infinity,
      padding: EdgeInsets.only(left: 20),
      child: Text(
        'the item i dont know what to say about it ,but it think it is quit good',
        style: textStyle.headline2,
        textAlign: TextAlign.start,
        maxLines: 1,
      ),
    );
  }

  Widget getPriceUI(price) {
    return Container(
      width: double.infinity,
      padding: EdgeInsets.only(left: 20, top: 5),
      child: Text(
        'Price: $price',
        style: TextStyle(
            fontSize: 15,
            fontFamily: 'Georgia',
            // fontStyle: FontStyle.,
            fontWeight: FontWeight.bold,
            color: Colors.orange[700]),
        textAlign: TextAlign.start,
        maxLines: 1,
      ),
    );
  }
}
